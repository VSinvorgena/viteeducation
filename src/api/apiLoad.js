export const loadAllItems = async (dest) => {
    const req = await fetch("https://min-api.cryptocompare.com/data/all/coinlist?summary=true")
    const res = await req.json()
    const coins = res.Data
    dest.allCoins = coins
}