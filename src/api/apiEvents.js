import {errorSubTickets, errorSubTicketsKeys, socket, subscribeToTicketWs, tickets, unsubscribeToTicketWs} from "./ws";

const onErrorSub = (ticket) => {
}

export const subscribeTicket = (ticket, cb) => {
    if (!tickets.has(ticket)) {
        tickets.set(ticket, {update: [cb], onError: [onErrorSub]})
        if (socket.readyState === socket.OPEN) {
            subscribeToTicketWs(ticket)
        } else {
            socket.addEventListener("open", () => {
                subscribeToTicketWs(ticket)
            }, {once: true})
        }
    }
}

export const unsubscribeTicket = (ticket) => {
    tickets.delete(ticket)
    unsubscribeToTicketWs(ticket)
}

export const onBTCUpdate = (currentTicket, newPrice) => {
    const errorTickets = errorSubTickets.get(currentTicket)
    errorTickets.forEach(ticket => {
        const currentPrice = errorSubTicketsKeys.get(ticket)
        const currentTick = tickets.get(ticket).update
        currentTick.forEach(func => func(ticket, newPrice * currentPrice))
    })
}
export const onInvalidSub = (wsMessageParam) =>{
    const item = wsMessageParam.split("~")[2]

    subscribeToTicketWs(item, "BTC")

    errorSubTickets.set("BTC", [...errorSubTickets.get("BTC") || [], item])
    errorSubTicketsKeys.set(item, "-")
    const errorHandlers = tickets.get(item).onError || []

    errorHandlers.forEach(func => func(item))
}