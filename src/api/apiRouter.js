const windowQueryData = Object.fromEntries(
    new URL(window.location).searchParams.entries()
)


export const getFromQuery = (dest, keys) =>{
    keys.forEach(queryParam => {
        if (typeof windowQueryData[queryParam] === 'undefined') {
            return
        }
        dest[queryParam] = windowQueryData[queryParam]
    })
}