import {onBTCUpdate, onInvalidSub} from "./apiEvents";

const API_KEY = "c8beb5633e51b8639856c42c53432e353a2b461896c11a4055cc7c505497019e"
const tokenUpdateType = "5"
const invalidType = "500"
const invalidSubMess = "INVALID_SUB"

export const tickets = new Map()
export const errorSubTickets = new Map()
export const errorSubTicketsKeys = new Map()
export const socket = new WebSocket(`wss://streamer.cryptocompare.com/v2?api_key=${API_KEY}`)

let isBtcSub = false

socket.addEventListener("message", (message) => {
    const {
        TYPE: type, FROMSYMBOL: currentTicket, PRICE: newPrice, MESSAGE: wsMessage,
        PARAMETER: wsMessageParam
    } = JSON.parse(message.data)
    if (type === invalidType && wsMessage === invalidSubMess) {
        onInvalidSub(wsMessageParam)
    }
    if (type === tokenUpdateType) {
        if (errorSubTicketsKeys.has(currentTicket)) {
            errorSubTicketsKeys.set(currentTicket, newPrice)
            return
        }
        if (errorSubTickets.has(currentTicket)) {
            onBTCUpdate(currentTicket, newPrice)

        }

        const updateHandlers = tickets.get(currentTicket).update || []
        updateHandlers.forEach(func => func(currentTicket, newPrice))
    }

})

const sendToWs = (message) => {
    socket.send(message)
}

export const subscribeToTicketWs = (item, currency = "USD") => {
    if (item === "BTC" && isBtcSub) {
        return
    }
    if (item === "BTC") {
        isBtcSub = true
    }
    const message = JSON.stringify({
        "action": "SubAdd",
        "subs": [`5~CCCAGG~${item}~${currency}`]
    })
    sendToWs(message)
}
export const unsubscribeToTicketWs = (item) => {
    const message = JSON.stringify({
        "action": "SubRemove",
        "subs": [`5~CCCAGG~${item}~USD`]
    })
    sendToWs(message)
}

window.errorSubTicketsKeys = errorSubTicketsKeys
window.errorSubTickets = errorSubTickets
window.isBtcSub = isBtcSub